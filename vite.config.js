import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import graphqlLoader from "vite-plugin-graphql-loader"
import vue from '@vitejs/plugin-vue'

const base = './'

// https://vitejs.dev/config/
export default defineConfig({
  base,
  plugins: [graphqlLoader(), vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
